---
title: "About Me"
date: 2019-08-04T18:08:58-07:00
---

My name is Mike. I solve problems using computers. There is much more about my
[coding activities](/code) below.

I am thoroughly a geek. Besides spending most of my time working with and
trying to understand software, I also enjoy speculative fiction, science in
general, and constantly learning new things, both useful and trivial. A few of
my favorite non-computing topics are linguistics, psychology, and scientific
skepticism.

I also enjoy running and exercise; movies, both cerebral and idiotic; beers,
both ale and lager; probably some other things.
