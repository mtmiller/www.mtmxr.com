---
title: "Coding"
date: 2019-09-28T15:23:22-07:00
---

I write a lot of code, both professionally and on my own time. I am passionate
about free software (known to some as open source software) and contribute a
lot of time and energy into projects and organizations that I am interested
in.

Most of the projects that I work on or contribute to are hosted on
[GitHub](https://github.com/mtmiller),
[GitLab](https://gitlab.com/mtmiller),
[Debian Salsa](https://salsa.debian.org/mtmiller), or
[Bitbucket](https://bitbucket.org/mtmiller),

I always enjoy talking about and learning about software, so feel free to get
in touch with me on any of these sites or [contact me](/contact) in other
ways.
